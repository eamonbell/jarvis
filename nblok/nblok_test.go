package nblok

import (
	"io/ioutil"
	"net/http"
	"strconv"
	"testing"
)

var urls = []string{
	"https://golang.org",
	"https://godoc.org",
	"https://play.golang.org",
	"https://gopl.io",
	"https://golang.org",
	"https://godoc.org",
	"https://play.golang.org",
	"https://gopl.io",
}

func httpGetBody(url string) func() ([]byte, error) {
	return func() ([]byte, error) {
		resp, err := http.Get(url)
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()
		return ioutil.ReadAll(resp.Body)
	}
}

func incomingURLs() <-chan string {
	ch := make(chan string)
	go func() {
		for _, url := range urls {
			ch <- url
		}
		close(ch)
	}()
	return ch
}

func BenchmarkNewCache(b *testing.B) {
	cache := NewCache()
	for i := 0; i < b.N; i++ {
		_, _ = cache.Get(strconv.Itoa(i), func() ([]byte, error) {
			return []byte(strconv.Itoa(i)), nil
		})
	}
}

