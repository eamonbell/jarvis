package cache

import (
	"testing"
	"time"
)

func TestNode_HasExpired(t *testing.T) {
	cache := NewCache()
	cache.Set("test", 1, time.Second)
	time.Sleep(time.Millisecond * 1200)
	val := cache.Get("test")
	if val != nil {
		t.Error("the node in the cache did not expire")
	}
}

func TestCache_Get(t *testing.T) {
	cache := NewCache()
	cache.Set("test", 1, time.Second)
	val := cache.Get("test")
	if val != 1 {
		t.Error("the cache did not return a value of 1")
	}
}
