package cache

import (
	"fmt"
	"time"
)

// INode is an interface that defines the methods that a node must implement
type INode interface {
	SetExpiration(expires time.Duration)
	HasExpired() bool
	String() string
}

// Node is an element in a double linked list.
type Node struct {
	INode
	Key       string
	Value     interface{}
	ExpiresAt int64

	PrevNode *Node
	NextNode *Node
}

// Returns a string representation of a Node struct
func (n *Node) String() string {
	return fmt.Sprintf("Key: %s, Value: %s, Expired: %t", n.Key, n.Value, n.HasExpired())
}

// SetExpiration sets the time that a node in the cache will expire
func (n *Node) SetExpiration(expires time.Duration) {
	n.ExpiresAt = time.Now().Add(expires).Unix()
}

func (n *Node) HasExpired() bool {
	return n.ExpiresAt <= time.Now().Unix()
}
