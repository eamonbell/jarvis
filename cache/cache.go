// Package cache is used for creating a cache that implements the LRU algorithm
package cache

import (
	"time"
)

// ICache is an interface that defines the methods a Cache must implement
type ICache interface {
	Set(key string, value interface{}, expires time.Duration)
	Get(key string) interface{}
	String() string
}

// Cache implements the ICache interface.
type Cache struct {
	ICache

	DefaultLifespan time.Duration
	Map             map[string]*Node
	FirstNode       *Node
	LastNode        *Node
	//sync.Mutex
}

// Returns a string representation of a Cache struct
func (c *Cache) String() string {
	return ""
}

// NewCache returns a Cache struct with the given capacity
func NewCache() *Cache {
	return &Cache{
		Map:       make(map[string]*Node),
		FirstNode: nil,
		LastNode:  nil,
	}
}

// Set adds a Node to the Cache map.
func (c *Cache) Set(key string, value interface{}, expires time.Duration) {
	c.removeNode(key)
	c.addNodeToHead(key, value, expires)
}

// Get returns the value for the given key from the cache
func (c *Cache) Get(key string) interface{} {
	node := c.Map[key]
	if node == nil {
		return nil
	}
	c.removeNode(key)
	if node.HasExpired() {
		return nil
	}
	c.moveNodeToHead(node)
	return node.Value
}

func (c *Cache) addNodeToHead(key string, value interface{}, expires time.Duration) {
	node := &Node{
		Key:      key,
		Value:    value,
		NextNode: c.FirstNode,
		PrevNode: nil,
	}
	node.SetExpiration(expires)
	if c.FirstNode != nil {
		c.FirstNode.PrevNode = node
	}
	if c.LastNode == nil {
		c.LastNode = node
	}
	c.FirstNode = node
	c.Map[key] = node
}

func (c *Cache) moveNodeToHead(node *Node) {
	node.NextNode = c.FirstNode
	node.PrevNode = nil
	if c.FirstNode != nil {
		c.FirstNode.PrevNode = node
	}
	if c.LastNode == nil {
		c.LastNode = node
	}
	c.FirstNode = node
	c.Map[node.Key] = node
}

func (c *Cache) removeNode(key string) {
	node := c.Map[key]
	if node == nil {
		return
	}
	if key == c.FirstNode.Key {
		c.FirstNode = c.FirstNode.NextNode
	}
	if key == c.LastNode.Key {
		c.LastNode = c.LastNode.PrevNode
	}
	if node.PrevNode != nil {
		node.PrevNode.NextNode = node.NextNode
	}
	if node.NextNode != nil {
		node.NextNode.PrevNode = node.PrevNode
	}
	delete(c.Map, key)
}
