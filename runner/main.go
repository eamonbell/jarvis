package main

import (
	"gitlab.com/eamonbell/jarvis"
	"gitlab.com/eamonbell/jarvis/serv"
	"log"
)

var App *jarvis.App

func main() {
	App = jarvis.New("Test", "/")
	r := App.Server
	r.Use(Logger())
	r.Handle("POST", "/main/:test", Handle)

	App.Run()
}

func Handle(s *serv.State) {
	p := s.GetParam("test")
	res := &struct {
		Message string `json:"message"`
	}{Message: "this works " + p}
	log.Println("In handler " + p)
	s.JSON(200, &res)
}

func Logger() serv.Middleware {
	return func(s *serv.State) {
		log.Println("Here")
	}
}
