package jarvis

import (
	"gitlab.com/eamonbell/jarvis/cache"
	"gitlab.com/eamonbell/jarvis/serv"
	"log"
	"time"
)

type App struct {
	BasePath string
	AppName  string

	config map[string]interface{}
	cache  *cache.Cache
	Server *serv.Router
}

func New(name, basePath string) *App {
	app := &App{
		BasePath: basePath,
		AppName:  name,
		cache:    cache.NewCache(),
	}
	app.Server = serv.New()
	return app
}

func (a *App) Run() {
	log.Fatal(a.Server.Run(":8080"))
}

func (a *App) AddRoutes(routes Routes) {
	for _, r := range routes {
		a.Server.Handle(r.Method, r.Path, r.RequestHandler, r.Middleware...)
	}
}

func (a *App) SetConfig(c map[string]interface{}) {
	a.config = c
}

func (a *App) AddCacheItem(key string, value interface{}, expires time.Duration) {
	a.cache.Set(key, value, expires)
}

func (a *App) GetCacheItem(key string) interface{} {
	return a.cache.Get(key)
}

func (a *App) ResetCache() {
	a.cache = cache.NewCache()
}
