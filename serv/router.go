package serv

import (
	"sync"
)

type IRoute interface {
	Use(...Middleware) IRoute
	Handle(string, string, RequestHandler, ...Middleware) IRoute
	Any(string, RequestHandler, ...Middleware) IRoute
	GET(string, RequestHandler, ...Middleware) IRoute
	POST(string, RequestHandler, ...Middleware) IRoute
	DELETE(string, RequestHandler, ...Middleware) IRoute
	PATCH(string, RequestHandler, ...Middleware) IRoute
	PUT(string, RequestHandler, ...Middleware) IRoute
	OPTIONS(string, RequestHandler, ...Middleware) IRoute
	HEAD(string, RequestHandler, ...Middleware) IRoute
}

type Router struct {
	RouteGroup

	RedirectTrailingSlash  bool
	ReturnMethodNotAllowed bool
	RedirectFixedPath      bool
	pool                   sync.Pool
	RouteTree              nodeTrees
}

type RouteInfo struct {
	Method         string
	Path           string
	Middleware     []string
	RequestHandler string
}
