package serv

type nodeTree struct {
	Method string
	root *node
}

type nodeTrees []nodeTree

func (n nodeTrees) get(method string) *node {
	for _, tree := range n {
		if tree.Method == method {
			return tree.root
		}
	}
	return nil
}
