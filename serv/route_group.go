package serv

import (
	"net/http"
	"regexp"
)

type IRouter interface {
	IRoute
	Group(string, ...Middleware) *RouteGroup
}

// RouteGroup is a group of routes to be handled by the router
type RouteGroup struct {
	Middleware MiddlewareChain
	basePath   string
	router     *Router
	root       bool
}

var _ IRouter = &RouteGroup{}

// Use applies middleware a group of routes
func (rg *RouteGroup) Use(middleware ...Middleware) IRoute {
	rg.Middleware = append(rg.Middleware, middleware...)
	return rg.returnRouteGroup()
}

func (rg *RouteGroup) Handle(method string, relativePath string, requestHandler RequestHandler, chain ...Middleware) IRoute {
	if matches, err := regexp.MatchString("^[A-Z]+$", method); !matches || err != nil {
		panic("http method " + method + " is not valid")
	}
	return rg.handle(method, relativePath, requestHandler, chain)
}

func (rg *RouteGroup) Group(relativePath string, middleware ...Middleware) *RouteGroup {
	return &RouteGroup{
		Middleware: rg.mergeMiddleware(middleware),
		basePath:   rg.buildAbsolutePath(relativePath),
		router:     rg.router,
	}
}

func (rg *RouteGroup) returnRouteGroup() IRoute {
	if rg.root {
		return rg.router
	}
	return rg
}

func (rg *RouteGroup) mergeMiddleware(middleware MiddlewareChain) MiddlewareChain {
	finalSize := len(rg.Middleware) + len(middleware)
	mergedMiddleware := make(MiddlewareChain, finalSize)
	copy(mergedMiddleware, rg.Middleware)
	copy(mergedMiddleware[len(rg.Middleware):], middleware)
	return mergedMiddleware
}

func (rg *RouteGroup) buildAbsolutePath(relativePath string) string {
	return joinPaths(rg.basePath, relativePath)
}

func (rg *RouteGroup) handle(method string, relativePath string, requestHandler RequestHandler, chain MiddlewareChain) IRoute {
	absolutePath := rg.buildAbsolutePath(relativePath)
	middleware := rg.mergeMiddleware(chain)
	rg.router.addRoute(method, absolutePath, requestHandler, middleware)
	return rg.returnRouteGroup()
}

func (rg *RouteGroup) GET(relativePath string, requestHandler RequestHandler, chain ...Middleware) IRoute {
	return rg.handle("GET", relativePath, requestHandler, chain)
}

func (rg *RouteGroup) POST(relativePath string, requestHandler RequestHandler, chain ...Middleware) IRoute {
	return rg.handle("POST", relativePath, requestHandler, chain)
}

func (rg *RouteGroup) DELETE(relativePath string, requestHandler RequestHandler, chain ...Middleware) IRoute {
	return rg.handle("DELETE", relativePath, requestHandler, chain)
}

func (rg *RouteGroup) PATCH(relativePath string, requestHandler RequestHandler, chain ...Middleware) IRoute {
	return rg.handle("PATCH", relativePath, requestHandler, chain)
}

func (rg *RouteGroup) PUT(relativePath string, requestHandler RequestHandler, chain ...Middleware) IRoute {
	return rg.handle("PUT", relativePath, requestHandler, chain)
}

func (rg *RouteGroup) OPTIONS(relativePath string, requestHandler RequestHandler, chain ...Middleware) IRoute {
	return rg.handle("OPTIONS", relativePath, requestHandler, chain)
}

func (rg *RouteGroup) Any(string, RequestHandler, ...Middleware) IRoute {
	panic("implement me")
}

func (rg *RouteGroup) HEAD(relativePath string, requestHandler RequestHandler, chain ...Middleware) IRoute {
	panic("implement me")
}

func (rg *RouteGroup) StaticFile(string, string) IRoute {
	panic("implement me")
}

func (rg *RouteGroup) Static(string, string) IRoute {
	panic("implement me")
}

func (rg *RouteGroup) StaticFS(string, http.FileSystem) IRoute {
	panic("implement me")
}
