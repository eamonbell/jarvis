package render

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type JSON struct {
	Data interface{}
}

type ASCIIJSON struct {
	Data interface{}
}

var jsonContentType = []string{"application/json; charset=utf-8"}
var jsonASCIIContentType = []string{"application/json"}

func (r JSON) Render(w http.ResponseWriter) (err error) {
	if err = WriteJSON(w, r.Data); err != nil {
		panic(err)
	}
	return
}

func (r JSON) WriteContentType(w http.ResponseWriter) {
	writeContentType(w, jsonContentType)
}

func WriteJSON(w http.ResponseWriter, obj interface{}) error {
	writeContentType(w, jsonContentType)
	jsonBytes, err := json.Marshal(obj)
	if err != nil {
		return err
	}
	_, _ = w.Write(jsonBytes)
	return nil
}

func (r ASCIIJSON) Render(w http.ResponseWriter) (err error) {
	r.WriteContentType(w)
	ret, err := json.Marshal(r.Data)
	if err != nil {
		return err
	}

	var buffer bytes.Buffer
	for _, r := range string(ret) {
		cvt := string(r)
		if r >= 128 {
			cvt = fmt.Sprintf("\\u%04x", int64(r))
		}
		buffer.WriteString(cvt)
	}

	_, _ = w.Write(buffer.Bytes())
	return nil
}

func (r ASCIIJSON) WriteContentType(w http.ResponseWriter) {
	writeContentType(w, jsonASCIIContentType)
}
