package render

import (
	"net/http"
)

type Render interface {
	Render(http.ResponseWriter) error
	WriteContentType(w http.ResponseWriter)
}

var (
	_ Render = JSON{}
	//_ Render     = String{}
	//_ Render     = Redirect{}
	//_ Render     = Data{}
	//_ Render     = Reader{}
	_ Render = ASCIIJSON{}
)

func writeContentType(w http.ResponseWriter, value []string) {
	header := w.Header()
	if val := header["Content-Type"]; len(val) == 0 {
		header["Content-Type"] = value
	}
}
