package serv

import (
	"gitlab.com/eamonbell/jarvis/serv/render"
	"net/http"
)

type State struct {
	Request         *http.Request
	URIParams       URIParams
	MiddlewareChain MiddlewareChain
	RequestHandler  RequestHandler
	Writer          ResponseWriter
	Store           map[string]interface{}
	Aborted         bool
	middlewareIndex int8
	memWriter       responseWriter
	router          *Router
}

func (s *State) reset() {
	s.Writer = &s.memWriter
	s.URIParams = s.URIParams[0:0]
	s.MiddlewareChain = nil
	s.RequestHandler = nil
	s.Store = make(map[string]interface{})
	s.Aborted = false
	s.middlewareIndex = -1
}

func (s *State) Next() {
	s.middlewareIndex++
	for i := int8(len(s.MiddlewareChain)); s.middlewareIndex < i; s.middlewareIndex++ {
		if s.MiddlewareChain[s.middlewareIndex] == nil {
			continue
		}
		s.MiddlewareChain[s.middlewareIndex](s)
	}
	if !s.Aborted && s.middlewareIndex == int8(len(s.MiddlewareChain)) && s.RequestHandler != nil {
		s.RequestHandler(s)
	}
}

func (s *State) Abort() {
	s.Aborted = true
}

// Status sets the HTTP response code.
func (s *State) Status(code int) {
	s.memWriter.WriteHeader(code)
}

func (s *State) Render(code int, r render.Render) {
	s.Status(code)
	if !bodyAllowedForStatus(code) {
		r.WriteContentType(s.Writer)
		s.Writer.WriteHeaderNow()
		return
	}
	if err := r.Render(s.Writer); err != nil {
		panic(err)
	}
}

func (s *State) JSON(code int, obj interface{}) {
	s.Render(code, render.JSON{Data: obj})
}

func (s *State) GetParam(key string) string {
	p, _ := s.URIParams.Get(key)
	return p
}

func bodyAllowedForStatus(status int) bool {
	switch {
	case status >= 100 && status <= 199:
		return false
	case status == http.StatusNoContent:
		return false
	case status == http.StatusNotModified:
		return false
	}
	return true
}
