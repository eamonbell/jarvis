package serv

// Middleware is a function that accepts the current state
// and runs before the designated request handler
type Middleware func(*State)

// MiddlewareChain is a list of middleware functions to be run
type MiddlewareChain []Middleware