package serv

import (
	"net/url"
)

type pathType int8

const (
	static pathType = iota // default
	base
	param
	catch
)

type node struct {
	path            string
	indices         string
	priority        int
	wildChild       bool
	middlewareChain MiddlewareChain
	requestHandler  RequestHandler
	children        []*node
	pathType        pathType
	maxParamCount   int
}

func (n *node) incrementChildPriority(pos int) int {
	n.children[pos].priority++
	prio := n.children[pos].priority
	// adjust position (move to front)
	newPos := pos
	for newPos > 0 && n.children[newPos-1].priority < prio {
		n.children[newPos-1], n.children[newPos] = n.children[newPos], n.children[newPos-1]
		newPos--
	}
	if newPos != pos {
		n.indices = n.indices[:newPos] + // unchanged prefix, might be empty
			n.indices[pos:pos+1] + // the index char we move
			n.indices[newPos:pos] + n.indices[pos+1:] // rest without char at 'pos'
	}
	return newPos
}

//noinspection GoAssignmentToReceiver
func (n *node) addRoute(path string, handler RequestHandler, chain MiddlewareChain) {
	fullPath := path
	n.priority++
	paramCount := getURIParamCount(path)

	if len(path) > 0 || len(n.children) > 0 {
	walk:
		for {
			if paramCount > n.maxParamCount {
				n.maxParamCount = paramCount
			}
			i := 0
			max := min(len(path), len(n.path)) // the max length that
			for i < max && path[i] == n.path[i] {
				i++
			}
			if i < len(n.path) {
				child := node{
					path:            n.path[i:],
					wildChild:       n.wildChild,
					indices:         n.indices,
					children:        n.children,
					middlewareChain: n.middlewareChain,
					requestHandler:  n.requestHandler,
					priority:        n.priority - 1,
				}

				for _, c := range child.children {
					if c.maxParamCount > child.maxParamCount {
						child.maxParamCount = c.maxParamCount
					}
				}

				n.children = []*node{&child}
				n.indices = string([]byte{n.path[i]})
				n.path = path[:i]
				n.middlewareChain = nil
				n.requestHandler = nil
				n.wildChild = false
			}

			if i < len(path) {
				path = path[i:]
				if n.wildChild {
					n = n.children[0]
					n.priority++

					if paramCount > n.maxParamCount {
						n.maxParamCount = paramCount
					}
					paramCount--
					if len(path) >= len(n.path) && n.path == path[:len(n.path)] {
						if len(n.path) >= len(path) || path[len(n.path)] == '/' {
							continue walk
						}
					}
				}

				c := path[0]
				if n.pathType == param && c == '/' && len(n.children) == 1 {
					n = n.children[0]
					n.priority++
					continue walk
				}

				for i := 0; i < len(n.indices); i++ {
					if c == n.indices[i] {
						i = n.incrementChildPriority(i)
						n = n.children[i]
						continue walk
					}
				}

				if c != ':' {
					n.indices += string([]byte{c})
					child := &node{
						maxParamCount: paramCount,
					}
					n.children = append(n.children, child)
					n.incrementChildPriority(len(n.indices) - 1)
					n = child
				}
				n.addNodeToTree(paramCount, path, fullPath, handler, chain)
				return
			} else if i == len(path) {
				if n.middlewareChain != nil {
					panic("handlers are already registered for path '" + fullPath + "'")
				}
				n.middlewareChain = chain
				n.requestHandler = handler
			}
			return
		}
	} else {
		n.addNodeToTree(paramCount, path, fullPath, handler, chain)
		n.pathType = base
	}
}

//noinspection GoAssignmentToReceiver
func (n *node) addNodeToTree(paramCount int, path string, fullPath string, handler RequestHandler, chain MiddlewareChain) {
	var offset int
	for i, max := 0, len(path); paramCount > 0; i++ {
		c := path[i]
		if c != ':' {
			continue
		}
		end := i + 1 // find wildcard end (either '/' or path end)
		for end < max && path[end] != '/' {
			switch path[end] {
			case ':': // the wildcard name must not contain ':'
				panic("only one wildcard per path segment is allowed, has: '" +
					path[i:] + "' in path '" + fullPath + "'")
			default:
				end++
			}
		}
		if len(n.children) > 0 {
			panic("wildcard route '" + path[i:end] +
				"' conflicts with existing children in path '" + fullPath + "'")
		}
		if end-i < 2 {
			panic("wildcards must be named with a non-empty name in path '" + fullPath + "'")
		}
		if c == ':' {
			if i > 0 {
				n.path = path[offset:i]
				offset = i
			}
			child := &node{
				pathType:      param,
				maxParamCount: paramCount,
			}
			n.children = []*node{child}
			n.wildChild = true
			n = child
			n.priority++
			paramCount--

			if end < max {
				n.path = path[offset:end]
				offset = end
				child := &node{
					maxParamCount: paramCount,
					priority:      1,
				}
				n.children = []*node{child}
				n = child
			}
		} else {
			i--
			n.path = path[offset:i]
			child := &node{
				pathType:      catch,
				maxParamCount: 1,
				wildChild:     true,
			}
			n.children = []*node{child}
			n.indices = string(path[i])
			n = child
			n.priority++
			child = &node{ // second node: node holding the variable
				path:            path[i:],
				pathType:        catch,
				maxParamCount:   1,
				middlewareChain: chain,
				requestHandler:  handler, //TODO: Check on this
				priority:        1,
			}
			n.children = []*node{child}

			return
		}
	}

	n.path = path[offset:]
	n.middlewareChain = chain
	n.requestHandler = handler
}

//noinspection GoAssignmentToReceiver
func (n *node) getValue(path string, params URIParams, unescape bool) (handler RequestHandler, chain MiddlewareChain, p URIParams, tsr bool) {
	p = params
walk:
	for {
		if len(path) > len(n.path) {
			if path[:len(n.path)] == n.path {
				path = path[len(n.path):]
				if !n.wildChild {
					c := path[0]
					for i := 0; i < len(n.indices); i++ {
						if c == n.indices[i] {
							n = n.children[i]
							continue walk
						}
					}
					tsr = path == "/" && n.requestHandler != nil
					return
				}
				n = n.children[0]
				switch n.pathType {
				case param:
					end := 0
					for end < len(path) && path[end] != '/' {
						end++
					}
					if cap(p) < int(n.maxParamCount) { // save param value
						p = make(URIParams, 0, n.maxParamCount)
					}
					i := len(p)
					p = p[:i+1] // expand slice within preallocated capacity
					p[i].Key = n.path[1:]
					val := path[:end]
					if unescape {
						var err error
						if p[i].Value, err = url.QueryUnescape(val); err != nil {
							p[i].Value = val // fallback, in case of error
						}
					} else {
						p[i].Value = val
					}

					// we need to go deeper!
					if end < len(path) {
						if len(n.children) > 0 {
							path = path[end:]
							n = n.children[0]
							continue walk
						}

						// ... but we can't
						tsr = len(path) == end+1
						return
					}
					chain = n.middlewareChain
					handler = n.requestHandler
					if chain != nil || handler != nil {
						return
					}
					if len(n.children) == 1 {
						n = n.children[0]
						tsr = n.path == "/" && (n.middlewareChain != nil || n.requestHandler != nil)
					}
					return
				case catch:
					if cap(p) < int(n.maxParamCount) { // save param value
						p = make(URIParams, 0, n.maxParamCount)
					}
					i := len(p)
					p = p[:i+1] // expand slice within preallocated capacity
					p[i].Key = n.path[2:]
					if unescape {
						var err error
						if p[i].Value, err = url.QueryUnescape(path); err != nil {
							p[i].Value = path // fallback, in case of error
						}
					} else {
						p[i].Value = path
					}
					chain = n.middlewareChain
					handler = n.requestHandler
					return
				default:
					panic("invalid node type")
				}
			}
		} else if path == n.path {
			chain = n.middlewareChain
			handler = n.requestHandler
			if chain != nil || handler != nil {
				return
			}
			if path == "/" && n.wildChild && n.pathType != base { //TODO: Maybe delete this
				tsr = true
				return
			}
			for i := 0; i < len(n.indices); i++ {
				if n.indices[i] == '/' {
					n = n.children[i]
					tsr = (len(n.path) == 1 && n.requestHandler != nil) ||
						(n.pathType == catch && n.children[0].requestHandler != nil)
					return
				}
			}
			return
		}
		tsr = (path == "/") ||
			(len(n.path) == len(path)+1 && n.path[len(path)] == '/' &&
				path == n.path[:len(n.path)-1] && n.requestHandler != nil)
		return
	}
}
