package serv

type URIParam struct {
	Key string
	Value string
}

type URIParams []URIParam

func (p URIParams) Get(name string) (string, bool) {
	for _, entry := range p {
		if entry.Key == name {
			return entry.Value, true
		}
	}
	return "", false
}

func getURIParamCount(path string) int {
	var num int
	for i := 0; i < len(path); i++ {
		if path[i] != ':' && path[i] != '*' {
			continue
		}
		num++
	}

	return num
}