package serv

import (
	"net/http"
)

func New() *Router {
	router := &Router{
		RouteGroup: RouteGroup{
			Middleware: nil,
			basePath:   "/",
			root:       true,
		},
		RedirectTrailingSlash: true,

		RouteTree: make(nodeTrees, 0, 9),
	}

	router.RouteGroup.router = router
	router.pool.New = func() interface{} {
		return router.allocateState()
	}
	return router
}

func (r *Router) Run(address string) (err error) {
	err = http.ListenAndServe(address, r)
	return
}

func (r *Router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	s := r.pool.Get().(*State)
	s.memWriter.reset(w)
	s.Request = req
	s.reset()
	r.handleHTTPRequest(s)
	r.pool.Put(s)
}

func (r *Router) AddRoute(method string, path string, handler RequestHandler, chain ...Middleware) {
	root := r.RouteTree.get(method)
	if root == nil {
		root = new(node)
		r.RouteTree = append(r.RouteTree, nodeTree{Method: method, root: root})
	}
	root.addRoute(path, handler, chain)
}

func (r *Router) addRoute(method string, path string, handler RequestHandler, chain MiddlewareChain) {
	root := r.RouteTree.get(method)
	if root == nil {
		root = new(node)
		r.RouteTree = append(r.RouteTree, nodeTree{Method: method, root: root})
	}
	root.addRoute(path, handler, chain)
}

func (r *Router) allocateState() *State {
	return &State{router: r}
}

func (r *Router) handleHTTPRequest(s *State) {
	httpMethod := s.Request.Method
	path := s.Request.URL.Path
	t := r.RouteTree
	for _, tn := range t {
		if tn.Method != httpMethod {
			continue
		}
		root := tn.root
		handler, chain, params, _ := root.getValue(path, s.URIParams, true)
		if handler != nil || chain != nil {
			if handler != nil {
				s.RequestHandler = handler
			}
			if chain != nil {
				s.MiddlewareChain = chain
			}
			s.URIParams = params
			s.Next()
			s.memWriter.WriteHeaderNow()
			return
		}
		/*if httpMethod != "CONNECT" && path != "/" {
			if tsr && r.RedirectTrailingSlash {
				redirectTrailingSlash(s)
				return
			}
		}*/
		break

	}

	//serveError(s, http.StatusNotFound, []byte("404 page not found"))
}

func redirectTrailingSlash(s *State) {
	req := s.Request
	path := req.URL.Path
	code := http.StatusMovedPermanently // Permanent redirect, request with GET method
	if req.Method != "GET" {
		code = http.StatusTemporaryRedirect
	}
	req.URL.Path = path + "/"
	if length := len(path); length > 1 && path[length-1] == '/' {
		req.URL.Path = path[:length-1]
	}
	http.Redirect(s.Writer, req, req.URL.String(), code)
	s.memWriter.WriteHeaderNow()
}

func serveError(s *State, code int, defaultMessage []byte) {
	s.memWriter.status = code
	s.Next()
	if s.memWriter.Written() {
		return
	}
	if s.memWriter.Status() == code {
		s.memWriter.Header()["Content-Type"] = []string{"plain/text"}
		_, _ = s.Writer.Write(defaultMessage)
		return
	}
	s.memWriter.WriteHeaderNow()
	return
}
