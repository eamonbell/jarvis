package migrator

import (
	"fmt"
)

type RenameTable struct {
	name string
	newName string
}

func (t *RenameTable) TableSQL() string {
	return fmt.Sprintf("RENAME TABLE %s TO %s;", t.name, t.newName)
}