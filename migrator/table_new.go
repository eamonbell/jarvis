package migrator

import (
	"bytes"
	"fmt"
	"log"
	"text/template"
)

type NewTable struct {
	table
}

func (t *NewTable) String() string {
	res := "Name: %s\nColumns:\n"
	for _, c := range t.columnList {
		res += fmt.Sprintf("\t%s\n", c.(NewColumn))
	}
	res += "Foreign Keys:\n"
	for _, f := range t.foreignKeys {
		res += fmt.Sprintf("\t%s\n", f)
	}
	return fmt.Sprintf(res, t.name)
}

func (t NewTable) TableSQL() string {
	totalLength := len(t.columnList) + len(t.foreignKeys) - 1
	temp, _ := template.New("").Funcs(fns).Parse(newTableTemplate)
	var b bytes.Buffer
	err := temp.Execute(&b, struct {
		Name        string
		Columns     columnList
		ForeignKeys []foreignKey
		PrimaryKey string
		TotalLength int
	}{
		t.name,
		t.columnList,
		t.foreignKeys,
		t.primaryKeyString,
		totalLength,
	})
	if err != nil {
		log.Println(err)
	}
	return b.String()
}

func (t *NewTable) NewColumn(columnName string) *DataTypeBuilder {
	return &DataTypeBuilder{
		newTable:      t,
		builderType:   newTableBuilder,
		currentColumn: NewColumn{name: columnName, constraints: make(stringList, 0)},
	}
}

func (t *NewTable) PrimaryKey(keys ...string) {
	res := "("
	for i := 0; i < len(keys); i++ {
		res += keys[i]
		if i+1 == len(keys) {
			res += ")"
		} else {
			res += ", "
		}
	}
	t.primaryKeyString = res
}

func (t *NewTable) ForeignKey(columnName, refColumnName, refTableName string) *ForeignKeyBuilder {
	fk := foreignKey{
		columnName:    columnName,
		refColumnName: refColumnName,
		refTableName:  refTableName,
		deleteOption:  none,
		updateOption:  none,
	}
	fk.name = fmt.Sprintf("fk_%s_%s_%s", columnName, refTableName, refColumnName)
	return &ForeignKeyBuilder{newTable: t, currentForeignKey: fk, builderType: newTableBuilder}
}
