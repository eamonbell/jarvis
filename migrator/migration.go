package migrator

import (
	"strings"
)

// Migration ...
type Migration struct {
	Name         string
	GetSQLString func() string
}

func (m Migration) SnakeToUpperCamelCase() (camelCase string) {
	isToUpper := false
	for k, v := range m.Name {
		if k == 0 {
			camelCase = strings.ToUpper(string(m.Name[0]))
		} else {
			if isToUpper {
				camelCase += strings.ToUpper(string(v))
				isToUpper = false
			} else {
				if v == '_' || v == ' ' {
					isToUpper = true
				} else {
					camelCase += string(v)
				}
			}
		}
	}
	return

}

func (m Migration) SnakeToLowerCamelCase() (camelCase string) {
	isToUpper := false
	for k, v := range m.Name {
		if k == 0 {
			camelCase = strings.ToUpper(string(m.Name[0]))
		} else {
			if isToUpper {
				camelCase += strings.ToUpper(string(v))
				isToUpper = false
			} else {
				if v == '_' {
					isToUpper = true
				} else {
					camelCase += string(v)
				}
			}
		}
	}
	return

}
