package migrator

import (
	"bytes"
	"fmt"
	"log"
	"text/template"
)

type NewColumn struct {
	name         string
	dataType     string
	checkString  string
	defaultValue interface{}
	constraints  []string
	first        bool
	afterColumn  string
}

func (c NewColumn) ColumnSQL() string {
	temp, _ := template.New("").Parse(newColumnTemplate)
	var b bytes.Buffer
	err := temp.Execute(&b, struct {
		Name             string
		DataType         string
		ConstraintString string
		DefaultValue     string
		CheckString      string
	}{
		c.name,
		c.dataType,
		buildConstraintString(c.constraints...),
		buildDefaultString(c.defaultValue),
		buildCheckString(c.checkString),
	})
	if err != nil {
		log.Println(err)
	}
	return b.String()
}

func (c NewColumn) String() string {
	res := "Name: %s, DataType: %s, Constraints: %v"

	return fmt.Sprintf(res, c.name, c.dataType, c.constraints)
}

func (c NewColumn) SetDataType(dataType string) IDataColumn {
	c.dataType = dataType
	return c
}

func (c NewColumn) AddConstraint(conStr string) IDataColumn {
	found := false
	for _, s := range c.constraints {
		if s == conStr {
			found = true
		}
	}
	if !found {
		c.constraints = append(c.constraints, conStr)
	}
	return c
}

func (c NewColumn) GetDataType() string {
	return c.dataType
}

func (c NewColumn) SetDefaultValue(defaultValue interface{}) IDataColumn {
	c.defaultValue = defaultValue
	return c
}

func (c NewColumn) SetCheckCondition(checkStr string) IDataColumn {
	c.checkString = checkStr
	return c
}
