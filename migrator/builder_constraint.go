package migrator

import (
	"fmt"
	"strings"
)

type ConstraintBuilder struct {
	newTable      *NewTable
	existingTable *AlterTable
	currentColumn IDataColumn

	builderType builderType
}

func (b *ConstraintBuilder) NotNull() *ConstraintBuilder {
	b.currentColumn = b.currentColumn.AddConstraint("NOT NULL")
	return b
}

func (b *ConstraintBuilder) Unique() *ConstraintBuilder {
	b.currentColumn = b.currentColumn.AddConstraint("UNIQUE")
	return b
}

func (b *ConstraintBuilder) Check(checkStr string) *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetCheckCondition(checkStr)
	return b
}

func (b *ConstraintBuilder) Default(defaultValue interface{}) *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDefaultValue(defaultValue)
	return b
}

func (b *ConstraintBuilder) AutoIncrement() *ConstraintBuilder {
	if !strings.Contains(strings.ToLower(b.currentColumn.GetDataType()), "int") {
		panic(fmt.Sprintf("The data type '%s' cannot be auto incremented", b.currentColumn.GetDataType()))
	}
	b.currentColumn = b.currentColumn.AddConstraint("AUTO_INCREMENT")
	return b
}

func (b *ConstraintBuilder) Add() {
	if b.builderType == newTableBuilder {
		b.newTable.columnList = append(b.newTable.columnList, b.currentColumn)
	} else {
		b.existingTable.columnList = append(b.existingTable.columnList, b.currentColumn)
	}
}
