package migrator

import (
	"fmt"
)

type RenameColumn struct {
	name    string
	newName string
}

func (c RenameColumn) String() string {
	return fmt.Sprintf("Current Name: '%s', New Name: '%s'", c.name, c.newName)
}

func (c RenameColumn) ColumnSQL() string {
	return fmt.Sprintf("RENAME %s TO %s", c.name, c.newName)
}
