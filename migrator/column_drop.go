package migrator

import (
	"fmt"
)

type DropColumn struct {
	name string
}

func (c DropColumn) String() string {
	return fmt.Sprintf("Name: '%s'", c.name)
}

func (c DropColumn) ColumnSQL() string {
	return fmt.Sprintf("DROP COLUMN %s", c.name)
}
