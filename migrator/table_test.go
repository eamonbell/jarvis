package migrator

import (
	"fmt"
	"testing"
)

func TestCreateTable(t *testing.T) {
	tb := Create("testing")
	tb.NewColumn("col1").DataType("varchar(100)").NotNull().Add()

	//fmt.Println(tb)
}

func TestAlterTable(t *testing.T) {
	tb := Alter("testing")
	tb.AddColumn("col1").DataType("varchar(100)").NotNull().Add()
	tb.RenameColumn("col2", "col3")
	tb.DropColumn("username")
	tb.ChangeColumn("email").VarChar(320).Unique().Add()
	tb.DropPrimaryKey()
	tb.AddPrimaryKey("email", "username")
	fmt.Println(tb.TableSQL())
}

func TestNewTable_ForeignKey(t *testing.T) {
	tb := Create("testing")
	tb.NewColumn("id").Int().AutoIncrement().NotNull().Add()
	tb.NewColumn("name").VarChar(255).NotNull().Check("id > 2").Add()
	//tb.PrimaryKey("id")
	tb.ForeignKey("locCol", "refCol", "refTable").OnDeleteCascade().OnUpdateCascade().Add()
	tb.ForeignKey("locCol2", "refCol2", "refTable2").OnDeleteSetNull().OnUpdateSetDefault().Add()
	fmt.Println(tb.TableSQL())
}

func TestNewTable_PrimaryKey(t *testing.T) {
	tb := Create("testing")
	tb.NewColumn("id").Int().AutoIncrement().NotNull().Add()
	tb.NewColumn("name").VarChar(255).NotNull().Check("id > 2").Add()
	tb.PrimaryKey("id")
	fmt.Println(tb.TableSQL())
}

func TestDropTable_TableSQL(t *testing.T) {
	tb := Drop("testing")
	fmt.Println(tb.TableSQL())
}

func TestRenameTable_TableSQL(t *testing.T) {
	tb := Rename("testing", "new_testing")
	fmt.Println(tb.TableSQL())
}