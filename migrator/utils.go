package migrator

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

type stringList []string

func buildReferenceString(option referenceOption, base string) string {
	switch option {
	case noAction:
		base += "NO ACTION"
		break
	case restrict:
		base += "RESCRICT"
		break
	case setNull:
		base += "SET NULL"
		break
	case setDefault:
		base += "SET DEFAULT"
		break
	case cascade:
		base += "CASCADE"
		break
	}
	return base
}

func buildCheckString(checkStr string) (res string) {
	if strings.TrimSpace(checkStr) != "" {
		res = fmt.Sprintf("CHECK (%s)", checkStr)
	}

	return
}

func buildConstraintString(cons ...string) (res string) {
	for i := 0; i < len(cons); i++ {
		res += cons[i]
		if i+1 != len(cons) {
			res += " "
		}
	}
	return
}

func buildDefaultString(defVal interface{}) (def string) {
	dt := reflect.TypeOf(defVal)
	if dt == nil {
		return
	} else if reflect.ValueOf(defVal) == reflect.Zero(dt) {
		return
	}

	def = "DEFAULT "
	switch defVal.(type) {
	case int:
		def += strconv.Itoa(defVal.(int))
		break
	case string:
		temp := defVal.(string)
		if strings.Contains(temp, "(") && strings.Contains(temp, ")") {
			def += temp
		} else {
			def += "'" + temp + "'"
		}
		break
	case bool:
		def += strconv.FormatBool(defVal.(bool))
		break
	case float64:
		def += fmt.Sprintf("%f", defVal.(float64))
		break
	case float32:
		def += fmt.Sprintf("%f", defVal.(float32))
		break
	}
	return
}
