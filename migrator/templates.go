package migrator

import (
	"text/template"
)

var fns = template.FuncMap{
	"plus": func(x int, y int) int {
		return x + y
	},
	"getOnDelete": func(x referenceOption) string {
		return buildReferenceString(x, "ON DELETE ")
	},
	"getOnUpdate": func(x referenceOption) string {
		return buildReferenceString(x, "ON UPDATE ")
	},
	"isNewColumn": func(c IColumn) bool {
		_, ok := c.(NewColumn)
		return ok
	},
}

const newTableTemplate = `{{ $ctr := len .Columns -}}{{ $len := .TotalLength -}}
CREATE TABLE IF NOT EXISTS {{ .Name }} (
    {{- range $i, $c := .Columns }}
    {{ .ColumnSQL }}{{ if ne (plus $i 0) $len }},{{ end }}
    {{- end }}
	{{- range $j, $f := .ForeignKeys }}
    {{ .KeySQL }}{{ if ne (plus $j $ctr) $len }},{{ end }}
    {{- end }}
	{{- if ne .PrimaryKey "" }},{{ end }}
    {{ if ne .PrimaryKey "" }}PRIMARY KEY {{ .PrimaryKey }}{{ end }}
);`

const alterTableTemplate = `{{ $ctr := len .Columns -}}{{ $len := .TotalLength -}}
ALTER TABLE {{ .Name }}
    {{- range $i, $c := .Columns }}
    {{ if isNewColumn . }}ADD {{ end }}{{ .ColumnSQL }}{{ if ne (plus $i 0) $len }},{{ end }}
    {{- end }}
	{{- range $j, $f := .ForeignKeys }}
    ADD {{ .KeySQL }}{{ if ne (plus $j $ctr) $len }},{{ end }}
    {{- end }}
    {{- if .DropPrimaryKey }},{{ end }}
    {{ if .DropPrimaryKey }}DROP PRIMARY KEY{{ end }}
	{{- if ne .PrimaryKey "" }},{{ end }}
    {{ if ne .PrimaryKey "" }}ADD PRIMARY KEY {{ .PrimaryKey }}{{ end }};`

const newColumnTemplate = `{{ .Name }} {{ .DataType }}{{ if ne .ConstraintString "" }} {{ .ConstraintString }}{{ end }}
{{- if ne .DefaultValue "" }} {{ .DefaultValue }}{{ end }}
{{- if ne .CheckString "" }} {{ .CheckString }}{{ end }}`

const modifyColumnTemplate = `MODIFY {{ .Name }} {{ .DataType }}{{ if ne .ConstraintString "" }} {{ .ConstraintString }}{{ end }}
{{- if ne .DefaultValue "" }} {{ .DefaultValue }}{{ end }}
{{- if ne .CheckString "" }} {{ .CheckString }}{{ end }}`

const renameColumnTemplate = `{{ .Name }}{{ if ne .NewName "" }} {{ .NewName }}{{ end }} {{ .DataType }}`

const foreignKeyTemplate = `CONSTRAINT {{ .Name }} FOREIGN KEY ({{ .ColumnName }}) REFERENCES {{ .RefTable }} ({{ .RefColumn }})
{{- if ne .OnDelete 0 }} {{ .OnDelete | getOnDelete }}{{ end }}{{- if ne .OnUpdate 0 }} {{ .OnUpdate | getOnUpdate }}{{ end }}`
