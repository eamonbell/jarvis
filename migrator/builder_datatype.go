package migrator

import (
	"fmt"
)

type DataTypeBuilder struct {
	newTable      *NewTable
	existingTable *AlterTable
	currentColumn IDataColumn

	builderType builderType
}

func (b *DataTypeBuilder) Char(size int) *ConstraintBuilder {
	if size > 255 {
		panic("type char cannot have a length greater than 255")
	}
	b.currentColumn = b.currentColumn.SetDataType(fmt.Sprintf("CHAR(%d)", size))
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) VarChar(size int) *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDataType(fmt.Sprintf("VARCHAR(%d)", size))
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) Text() *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDataType("TEXT")
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) MediumText() *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDataType("MEDIUMTEXT")
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) LongText() *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDataType("LONGTEXT")
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) Enum(vals ...string) *ConstraintBuilder {
	res := "ENUM("
	for i := 0; i < len(vals); i++ {
		res += fmt.Sprintf("'%s'", vals[i])
		if i+1 != len(vals) {
			res += ", "
		} else {
			res += ")"
		}
	}
	b.currentColumn = b.currentColumn.SetDataType(res)
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) Bool() *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDataType("BOOL")
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) Int() *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDataType("INT")
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) BigInt() *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDataType("BIGINT")
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) Float(size, d int) *ConstraintBuilder {
	if size <= 0 || d <= 0 {
		panic("float must have a size and decimal greater than zero")
	}
	b.currentColumn = b.currentColumn.SetDataType(fmt.Sprintf("FLOAT(%d,%d)", size, d))
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) Double(size, d int) *ConstraintBuilder {
	if size <= 0 || d <= 0 {
		panic("double must have a size and decimal greater than zero")
	}
	b.currentColumn = b.currentColumn.SetDataType(fmt.Sprintf("DOUBLE(%d,%d)", size, d))
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) Decimal(size, d int) *ConstraintBuilder {
	if size <= 0 || d <= 0 {
		panic("decimal must have a size and decimal greater than zero")
	}
	b.currentColumn = b.currentColumn.SetDataType(fmt.Sprintf("DECIMAL(%d,%d)", size, d))
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) Date() *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDataType("DATE")
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) DateTime() *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDataType("DATETIME")
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) TimeStamp() *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDataType("TIMESTAMP")
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) Time() *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDataType("TIME")
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) Year() *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDataType("YEAR")
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}

func (b *DataTypeBuilder) DataType(dtype string) *ConstraintBuilder {
	b.currentColumn = b.currentColumn.SetDataType(dtype)
	return &ConstraintBuilder{newTable: b.newTable, existingTable: b.existingTable, currentColumn: b.currentColumn, builderType: b.builderType}
}
