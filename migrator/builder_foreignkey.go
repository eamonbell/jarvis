package migrator

import (
	"bytes"
	"fmt"
	"log"
	"text/template"
)

type referenceOption int8

const (
	none referenceOption = iota
	noAction
	restrict
	cascade
	setNull
	setDefault
)

type foreignKey struct {
	name          string
	columnName    string
	refTableName  string
	refColumnName string
	deleteOption  referenceOption
	updateOption  referenceOption
}

type ForeignKeyBuilder struct {
	newTable          *NewTable
	existingTable     *AlterTable
	currentForeignKey foreignKey
	builderType
}

func (f foreignKey) String() string {
	res := "Name: %s, AddColumn: %s, RefCol: %s, RefTable: %s"
	return fmt.Sprintf(res, f.name, f.columnName, f.refColumnName, f.refTableName)
}

func (f foreignKey) KeySQL() string {
	temp, _ := template.New("").Funcs(fns).Parse(foreignKeyTemplate)
	var b bytes.Buffer
	err := temp.Execute(&b, struct {
		Name       string
		ColumnName string
		RefColumn  string
		RefTable   string
		OnDelete   referenceOption
		OnUpdate   referenceOption
	}{
		f.name,
		f.refColumnName,
		f.refColumnName,
		f.refTableName,
		f.deleteOption,
		f.updateOption,
	})
	if err != nil {
		log.Println(err)
	}
	return b.String()
}

func (f *ForeignKeyBuilder) OnDeleteRestrict() *ForeignKeyBuilder {
	f.currentForeignKey.deleteOption = restrict
	return f
}

func (f *ForeignKeyBuilder) OnDeleteNoAction() *ForeignKeyBuilder {
	f.currentForeignKey.deleteOption = noAction
	return f
}

func (f *ForeignKeyBuilder) OnDeleteSetNull() *ForeignKeyBuilder {
	f.currentForeignKey.deleteOption = setNull
	return f
}

func (f *ForeignKeyBuilder) OnDeleteSetDefault() *ForeignKeyBuilder {
	f.currentForeignKey.deleteOption = setDefault
	return f
}

func (f *ForeignKeyBuilder) OnDeleteCascade() *ForeignKeyBuilder {
	f.currentForeignKey.deleteOption = cascade
	return f
}

func (f *ForeignKeyBuilder) OnUpdateRestrict() *ForeignKeyBuilder {
	f.currentForeignKey.updateOption = restrict
	return f
}

func (f *ForeignKeyBuilder) OnUpdateNoAction() *ForeignKeyBuilder {
	f.currentForeignKey.updateOption = noAction
	return f
}

func (f *ForeignKeyBuilder) OnUpdateSetNull() *ForeignKeyBuilder {
	f.currentForeignKey.updateOption = setNull
	return f
}

func (f *ForeignKeyBuilder) OnUpdateSetDefault() *ForeignKeyBuilder {
	f.currentForeignKey.updateOption = setDefault
	return f
}

func (f *ForeignKeyBuilder) OnUpdateCascade() *ForeignKeyBuilder {
	f.currentForeignKey.updateOption = cascade
	return f
}

func (f *ForeignKeyBuilder) Add() {
	if f.builderType == newTableBuilder {
		f.newTable.foreignKeys = append(f.newTable.foreignKeys, f.currentForeignKey)
	} else {
		f.existingTable.foreignKeys = append(f.existingTable.foreignKeys, f.currentForeignKey)
	}
}
