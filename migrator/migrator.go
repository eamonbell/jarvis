package migrator

func Create(tableName string) *NewTable {
	nt := &NewTable{}
	nt.name = tableName
	nt.columnList = make(columnList, 0)
	return nt
}

func Alter(tableName string) *AlterTable {
	et := &AlterTable{dropPrimaryKey:false}
	et.name = tableName
	et.columnList = make(columnList, 0)
	return et
}

func Drop(tableName string) *DropTable {
	return &DropTable{name: tableName}
}

func Rename(tableName, newTableName string) *RenameTable {
	return &RenameTable{name: tableName, newName: newTableName}
}