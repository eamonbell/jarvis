package migrator

import (
	"fmt"
)

type DropTable struct {
	name string
}

func (t *DropTable) TableSQL() string {
	return fmt.Sprintf("DROP TABLE %s IF EXISTS;", t.name)
}