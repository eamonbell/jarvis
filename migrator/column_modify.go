package migrator

import (
	"bytes"
	"log"
	"text/template"
)

type ModifyColumn struct {
	name         string
	dataType     string
	checkString  string
	defaultValue interface{}
	constraints  stringList
	first        bool
	afterColumn  string
}

func (c ModifyColumn) String() string {
	panic("implement me")
}

func (c ModifyColumn) ColumnSQL() string {
	temp, _ := template.New("").Parse(modifyColumnTemplate)
	var b bytes.Buffer
	err := temp.Execute(&b, struct {
		Name             string
		DataType         string
		ConstraintString string
		DefaultValue     string
		CheckString      string
	}{
		c.name,
		c.dataType,
		buildConstraintString(c.constraints...),
		buildDefaultString(c.defaultValue),
		buildCheckString(c.checkString),
	})
	if err != nil {
		log.Println(err)
	}
	return b.String()
}

func (c ModifyColumn) SetDataType(dataType string) IDataColumn {
	c.dataType = dataType
	return c
}

func (c ModifyColumn) GetDataType() string {
	return c.dataType
}

func (c ModifyColumn) AddConstraint(conStr string) IDataColumn {
	found := false
	for _, s := range c.constraints {
		if s == conStr {
			found = true
		}
	}
	if !found {
		c.constraints = append(c.constraints, conStr)
	}
	return c
}

func (c ModifyColumn) SetDefaultValue(defaultValue interface{}) IDataColumn {
	c.defaultValue = defaultValue
	return c
}

func (c ModifyColumn) SetCheckCondition(checkStr string) IDataColumn {
	c.checkString = checkStr
	return c
}
