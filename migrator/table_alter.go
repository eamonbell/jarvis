package migrator

import (
	"bytes"
	"fmt"
	"log"
	"strings"
	"text/template"
)

type AlterTable struct {
	table
	newName string
	dropPrimaryKey bool
}

func (t *AlterTable) String() string {
	res := "Name: %s\nColumns:\n"
	for _, c := range t.columnList {
		res += fmt.Sprintf("\t%s\n", c)
	}
	res += "Foreign Keys:\n"
	for _, f := range t.foreignKeys {
		res += fmt.Sprintf("\t%s\n", f)
	}
	return fmt.Sprintf(res, t.name)
}

func (t AlterTable) TableSQL() string {
	totalLength := len(t.columnList) + len(t.foreignKeys) - 1
	temp, _ := template.New("").Funcs(fns).Parse(alterTableTemplate)
	var b bytes.Buffer
	err := temp.Execute(&b, struct {
		Name        string
		Columns     columnList
		ForeignKeys []foreignKey
		DropPrimaryKey bool
		PrimaryKey string
		TotalLength int
	}{
		t.name,
		t.columnList,
		t.foreignKeys,
		t.dropPrimaryKey,
		t.primaryKeyString,
		totalLength,
	})
	if err != nil {
		log.Println(err)
	}
	return b.String()
}

func (t *AlterTable) AddColumn(columnName string) *DataTypeBuilder {
	return &DataTypeBuilder{
		existingTable: t,
		builderType:   existingTableBuilder,
		currentColumn: NewColumn{name: columnName, first: false},
	}
}

func (t *AlterTable) AddColumnAfter(columnName, afterColumn string) *DataTypeBuilder {
	if strings.TrimSpace(afterColumn) == "" {
		panic("The after column name cannot be empty")
	}
	return &DataTypeBuilder{
		existingTable: t,
		builderType:   existingTableBuilder,
		currentColumn: NewColumn{name: columnName, afterColumn: afterColumn, first: false},
	}
}

func (t *AlterTable) AddColumnFirst(columnName string) *DataTypeBuilder {
	return &DataTypeBuilder{
		existingTable: t,
		builderType:   existingTableBuilder,
		currentColumn: NewColumn{name: columnName, first: true},
	}
}

func (t *AlterTable) RenameTable(newName string) {
	t.newName = newName
}

func (t *AlterTable) RenameColumn(curName, newName string) {
	col := RenameColumn{name: curName, newName: newName}
	t.columnList = append(t.columnList, col)
}

func (t *AlterTable) DropColumn(columnName string) {
	col := DropColumn{name: columnName}
	t.columnList = append(t.columnList, col)
}

func (t *AlterTable) ChangeColumn(columnName string) *DataTypeBuilder {
	return &DataTypeBuilder{
		existingTable: t,
		builderType:   existingTableBuilder,
		currentColumn: ModifyColumn{name: columnName, first: false},
	}
}

func (t *AlterTable) AddPrimaryKey(keys ...string) {
	res := "("
	for i := 0; i < len(keys); i++ {
		res += keys[i]
		if i+1 == len(keys) {
			res += ")"
		} else {
			res += ", "
		}
	}
	t.primaryKeyString = res
}

func (t *AlterTable) DropPrimaryKey() {
	t.dropPrimaryKey = true
}