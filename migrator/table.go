package migrator

type iTable interface {
	TableSQL() string
}

type table struct {
	iTable
	name             string
	primaryKeyString string
	currentColumn    IColumn
	columnList       columnList
	foreignKeys      []foreignKey
}
