package migrator

type IBuilder interface {
	Add()
}

type builderType int8

const (
	newTableBuilder builderType = iota
	existingTableBuilder
)
