package migrator

import (
	"bytes"
	"fmt"
	"log"
	"reflect"
	"text/template"
)

type columnType int8

const (
	newColumn columnType = iota
	renameColumn
)

type IColumn interface {
	String() string
	ColumnSQL() string
}

type IDataColumn interface {
	IColumn
	SetDataType(dataType string) IDataColumn
	GetDataType() string
	AddConstraint(conStr string) IDataColumn
	SetDefaultValue(defaultValue interface{}) IDataColumn
	SetCheckCondition(checkStr string) IDataColumn
}

type columnList []IColumn

type constraintList []string

// OldColumn is a newTable column
type OldColumn struct {
	IColumn
	Name         string
	DataType     string
	defaultValue interface{}
	check        string
	generated    string
	constraints  []string
	comment      string
}

type ModifiedColumn struct {
	IColumn
	Name     string
	NewName  string
	DataType string
}

func (c OldColumn) ConstraintString() string {
	res := ""
	if len(c.constraints) > 0 {
		for i := 0; i < len(c.constraints); i++ {
			res += c.constraints[i]
			if i+1 != len(c.constraints) {
				res += " "
			}
		}
	}
	return res
}

func (c OldColumn) DefaultValueString() string {
	res := ""
	dt := reflect.TypeOf(c.defaultValue)
	if dt != nil {
		if reflect.ValueOf(c.defaultValue) != reflect.Zero(dt) {
			res = buildDefaultString(c.defaultValue)
		}
	}
	return res
}

func (c OldColumn) CheckString() string {
	res := ""
	if c.check != "" {
		res = fmt.Sprintf("CHECK(%s)", c.check)
	}
	return res
}

// SQL creates a sql string for column
func (c OldColumn) SQL() string {
	temp, _ := template.New("column").Parse(newColumnTemplate)
	var b bytes.Buffer
	err := temp.Execute(&b, c)
	if err != nil {
		log.Println(err)
	}
	return b.String()
}

func (c ModifiedColumn) SQL() string {
	temp, _ := template.New("column").Parse(renameColumnTemplate)
	var b bytes.Buffer
	err := temp.Execute(&b, c)
	if err != nil {
		log.Println(err)
	}
	return b.String()
}
