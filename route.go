package jarvis

import (
	"gitlab.com/eamonbell/jarvis/serv"
)

type Route struct {
	Method         string
	Path           string
	RequestHandler serv.RequestHandler
	Middleware     serv.MiddlewareChain
}

type Routes []Route
