package rotator

import (
	"fmt"
	"os"
	"strings"
	"sync"
	"time"
)

type Rotator struct {
	lock       sync.Mutex
	fileName   string
	logFile    *os.File
	currentDay int
}

// New returns a new Rotator for the given file name
func New(fileName string) (*Rotator, error) {
	r := &Rotator{
		fileName:   fileName,
		currentDay: time.Now().YearDay(),
	}
	if err := r.Rotate(); err != nil {
		return nil, err
	}
	return r, nil
}

// Write satisfies the io.Writer interface.
func (r *Rotator) Write(output []byte) (int, error) {
	r.lock.Lock()
	defer r.lock.Unlock()
	return r.logFile.Write(output)
}

// Rotate performs the log rotation
func (r *Rotator) Rotate() (err error) {
	r.lock.Lock()
	defer r.lock.Unlock()

	if r.logFile != nil {
		err = r.logFile.Close()
		r.logFile = nil
		if err != nil {
			return
		}
	}

	_, err = os.Stat(r.fileName)
	if err == nil {
		err = os.Rename(r.fileName, getNewFileName(r.fileName))
		if err != nil {
			return
		}
	}
	r.logFile, err = os.Create(r.fileName)
	return
}

func (r *Rotator) Watch() {
	nextTime := time.Now().Truncate(time.Hour)
	go func() {
		for {
			nextTime = nextTime.Add(time.Hour)
			time.Sleep(time.Until(nextTime))
			if time.Now().YearDay() != r.currentDay {
				_ = r.Rotate()
			}
		}
	}()
}

func getNewFileName(fileName string) string {
	fileName = strings.Replace(fileName, ".log", "", 1)
	now := time.Now()
	timeStr := fmt.Sprintf("%s-%d-%d", now.Month().String(), now.Day(), now.Year())

	return fmt.Sprintf("%s_%s.log", fileName, timeStr)
}
